//
//  HelloWorldViewController.swift
//  HelloWorld
//
//  Created by Dylan Law Wai Chun on 28/06/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import UIKit

class HelloWorldViewController: UIViewController {

    @IBOutlet weak var byeButton: UIButton!
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var hiButton: UIButton!
    @IBOutlet weak var moveLabelButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hiButton.layer.cornerRadius = 5
        byeButton.layer.cornerRadius = 5
        moveLabelButton.layer.cornerRadius = 5
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func saySomething(sender: AnyObject) {
        if let button: UIButton? = sender as? UIButton {
            UIView.animateWithDuration(0.5, animations: {
                if button == self.hiButton {
                    self.helloLabel.transform = CGAffineTransformIdentity
                    print("test")

                } else {
                    self.helloLabel.transform = CGAffineTransformRotate(CGAffineTransformIdentity, CGFloat(-180 * M_PI / 180))

                }
            })
        }
    }
    
    @IBAction func toggleView(sender: AnyObject) {
        UIView.animateWithDuration(0.5, animations: {
            self.helloLabel.frame.origin.y = self.view.frame.height - (self.helloLabel.frame.origin.y + self.helloLabel.frame.height)
            if self.helloLabel.frame.origin.y < self.view.frame.height - (self.helloLabel.frame.origin.y + self.helloLabel.frame.height) {
                self.helloLabel.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.5 , 0.5)
            } else {
                self.helloLabel.transform = CGAffineTransformScale(CGAffineTransformIdentity, 2 , 2)
            }
            self.view.layoutIfNeeded()
        })
    }
    
 
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
